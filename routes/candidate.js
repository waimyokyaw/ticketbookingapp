var express = require('express');
var _ = require('lodash');
var models = require('../models');
var router = express.Router();

router.get('/getjob', function (req, res) {
  //console.log(req.query);
  let userCurrentLocation = req.query.currentlocation.toLowerCase();

  let userCurrentLocationText = 'Your current location : ' + userCurrentLocation;
  console.log(userCurrentLocationText);

  models.Job.findAll({
    where: { locationref: userCurrentLocation }
  }).then(jobs => {
    console.log('JOBS =>> ', jobs);
    let resmsg = [];
    _.forEach(jobs, function (jobj) {
      resmsg.push({ "text": jobj.desc });
      resmsg.push({
        "attachment": {
          "type": "image",
          "payload": {
            "url": "https://rockets.chatfuel.com/assets/welcome.png"
          }
        }
      });
    });
    console.log(resmsg);
    res.send(resmsg);
  });

});

///GET All User.
router.get('/all', function (req, res) {
  models.Candidate.findAll().then(candidates => {
    res.send(candidates);
  });
});

router.post('/save', function (req, res) {
  //console.log('Direct from body: ', req.body);

  let detailinfo = JSON.stringify({
    location: req.body.currentlocation,
    phonenumber: req.body.phonenumber,
    timezone: req.body.timezone,
    updateddatetime:  Date.now()
  });
  console.log(detailinfo);
  let candidateInfoToCreate = {
    fbid: req.body['messenger user id'],
    fname: req.body['first name'],
    lname: req.body['last name'],
    gender: req.body.gender,
    sourceapp: 'fb messenger',
    details: detailinfo
  };
  models.Candidate.create(candidateInfoToCreate).then(newCandidate => {
    console.log('Candidate: ', newCandidate);
    res.send([{ "text": "Your information has been saved" }]);
  });
});

router.get('/postattr', function (req, res) {
  console.log('Test Attr : ', req.body.testattr);
  let testattr = req.body.testattr ? req.body.testattr : "+++++hello world";
  let msgposted = [{ "text": "This message is from another world " + testattr }];
  res.send(msgposted);
});

router.get('/', function (req, res) {
  res.render('candidates', {
    title: 'Candidates List'
  });
});

module.exports = router;