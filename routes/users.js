var express = require('express');
var models = require('../models');
var router = express.Router();

///Return user list view
router.get('/', function (req, res) {
  res.render('users', {
    title: 'Users List'
  });
});

///GET All User.
router.get('/all', function (req, res) {
  models.User.findAll({
    include: [{ model: models.Role, as: 'role' }], where: { isactive: true }
  }).then(users => {
    res.send(users);
  });
});

///Get User By Id
router.get('/:id', function (req, res) {
  return models.User.findByPk(req.params.id, {
    include: [{ model: models.Role, as: 'role' }]
  }).then(userobj => {
    res.send(userobj);
  });
});

///Create User and user role
router.post('/create', function (req, res) {
  let createValues = {
    fullname: req.body.fullname,
    age: req.body.age,
    isvip: req.body.isvip,
    roleid: req.body.roleid,
    notes: req.body.notes,
    isactive: true
  };
  models.User.create(createValues).then(newUser => {
    models.UserRole.create({
      UserId: newUser.id,
      RoleId: req.body.roleid
    });
    res.json({ created: newUser });
  });
});

///Update User and user role
router.post('/update', function (req, res) {
  let updateValues = {
    fullname: req.body.fullname,
    age: req.body.age,
    isvip: req.body.isvip,
    notes: req.body.notes
  };
  let whereValues = { where: { id: req.body.id } };
  models.User.update(updateValues, whereValues)
    .then(function ([rowsUpdate]) {
      models.UserRole.update({ RoleId: req.body.roleid }, { where: { UserId: req.body.id } });
      var result = { updated: rowsUpdate }
      res.json(result);
    }).catch(function (error) {
      console.log(error);
      res.json(error);
    });
});

//Delete User
router.post('/delete', function (req, res) {
  let updateValues = {
    isactive: false
  };
  let whereValues = { where: { id: req.body.id } };
  models.User.update(updateValues, whereValues)
    .then(function ([rowsUpdate]) {
      var result = { deactivated: rowsUpdate }
      res.json(result);
    }).catch(function (error) {
      console.log(error);
      res.json(error);
    });
});

module.exports = router;
