var express = require('express');
var router = express.Router();

/* Load user list on index */
router.get('/', function (req, res, next) {
  res.render('users', {
    title: 'Users List'
  });
});

module.exports = router;
