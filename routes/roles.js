var express = require('express');
var role = require('../models').Role;
var router = express.Router();

///Load all roles
router.get('/all', function (req, res) {
    role.findAll().then(roles => {
        res.send(roles);
    });
});

module.exports = router;
