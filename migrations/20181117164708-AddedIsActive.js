'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Users',
      'isactive',
      Sequelize.BOOLEAN
    );
  },

  down: (queryInterface, Sequelize) => {
   
  }
};
