'use strict';
module.exports = (sequelize, DataTypes) => {
  const Candidate = sequelize.define('Candidate', {
    fbid: DataTypes.STRING,
    email: DataTypes.STRING,
    fname: DataTypes.STRING,
    lname: DataTypes.STRING,
    gender: DataTypes.STRING,
    sourceapp: DataTypes.STRING,
    details: DataTypes.JSON
  }, {});
  Candidate.associate = function(models) {
    // associations can be defined here
  };
  return Candidate;
};