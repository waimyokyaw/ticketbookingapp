'use strict';
module.exports = function (sequelize, DataTypes) {
    const Role = sequelize.define('Role', {
        name: DataTypes.STRING,
    }, {});

    Role.associate = function (models) {
        Role.belongsToMany(models.User, { through: 'UserRole', as: 'user' });
    };

    return Role;
};