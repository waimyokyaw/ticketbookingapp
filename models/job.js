'use strict';
module.exports = (sequelize, DataTypes) => {
  const Job = sequelize.define('Job', {
    name: DataTypes.STRING,
    desc: DataTypes.STRING,
    locationref: DataTypes.STRING
  }, {sourceKey: 'locationref'} );
  Job.associate = function(models) {
    Job.hasOne(models.Location, {as: 'Location', foreignKey: 'reference'});
  };
  return Job;
};