'use strict';
module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('User', {
        fullname: DataTypes.STRING,
        age: DataTypes.INTEGER,
        notes: DataTypes.TEXT,
        isvip: DataTypes.BOOLEAN,
        isactive: DataTypes.BOOLEAN
    }, {});

    User.associate = function (models) {
        User.belongsToMany(models.Role, { through: 'UserRole', as: 'role' });
    };
    return User;
};