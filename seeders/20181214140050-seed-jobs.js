'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Jobs', [{
      name: 'Job in Yangon 1',
      desc: 'This is a sample job in yangon 1',
      locationref: 'yangon',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Job in Yangon 2',
      desc: 'This is a sample job in yangon 2',
      locationref: 'yangon',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Job in Mandalay 1',
      desc: 'This is a sample job in mandalay 1',
      locationref: 'mandalay',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Job in Mandalay 2',
      desc: 'This is a sample job in mandalay 2',
      locationref: 'mandalay',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Job in Taunggyi 1',
      desc: 'This is a sample job in Taunggyi 1',
      locationref: 'taunggyi',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Job in Taunggyi 2',
      desc: 'This is a sample job in Taunggyi 2',
      locationref: 'taunggyi',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
