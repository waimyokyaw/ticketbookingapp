'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Locations', [{
      name: 'Yangon',
      reference: 'yangon',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Mandalay',
      reference: 'mandalay',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Bago',
      reference: 'bago',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Taunggyi',
      reference: 'taunggyi',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
