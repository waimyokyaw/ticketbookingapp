'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      fullname: 'Wai Test',
      age: 20,
      isvip: true,
      notes: 'he is from planet earth',
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      fullname: 'Wai Test 2',
      age: 21,
      isvip: false,
      notes: 'hello from the planet earth',
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      fullname: 'Ticker manager Test',
      age: 30,
      isvip: true,
      notes: 'he is a ticket manager',
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
